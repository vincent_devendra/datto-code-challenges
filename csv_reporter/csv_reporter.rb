require 'csv'

class CSVReporter
  def self.run
    reporter = get_reporter

    loop do
      case choice
      when "e"
        puts "Bye!"
        return
      when "a"
        puts reporter.sort_by_age
      else
        puts "What last name do you want to search for?"
        last_name = gets.chomp
        ids = reporter.ids_for_last_name(last_name)
        puts ids.empty? ? "No matches" : ids
      end
    end
  end

  def initialize(csv_path)
    @csv_table = CSV.table(csv_path, headers: true, return_headers: true)
  end

  def ids_for_last_name(name)
    @csv_table.each_with_object([]) do |row, result|
      result << row[:id] if row[:last] == name
    end
  end

  def sort_by_age
    @csv_table.sort_by do |row| 
      case row[:age]
      when nil
        max_age + 1
      when "Age"
        0
      else
        row[:age]
      end
    end
  end

  private

  def max_age
    @csv_table.max_by { |row| row[:age].to_i }[:age].to_i
  end

  def self.get_reporter
    loop do
      puts "Please enter the path for your csv file:"
      csv_path = gets.chomp

      begin
        path = CSVReporter.new(csv_path)
      rescue => e
        puts e.message
      else
        return path
      end
    end
  end

  def self.choice
    loop do
      puts "Choose find ids by (l)ast name, sort rows by (a)ge, or (e)xit"
      choice = gets.chomp
      return choice if %w(l a e).include?(choice)
    end
  end
end

CSVReporter.run
