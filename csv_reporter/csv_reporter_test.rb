require 'minitest/autorun'
require_relative 'csv_reporter'

class CSVReporterTest < MiniTest::Test
  def test_last_name_query
    csv_path = "sample_csv.csv"
    last_name = "Robiner"
    expected = [3, 6, 7]
    actual = CSVReporter.new(csv_path).ids_for_last_name(last_name)
    assert_equal(expected, actual)
  end
end
