require 'minitest/autorun'
require_relative 'deserialize'

class DeserializeTest < MiniTest::Test
  def test_throws_error_if_input_invalid
    input = "#"
    assert_raises(CustomTextFormat::Deserializer::InvalidFormatError) do
      CustomTextFormat.deserialize(input)
    end
  end

  def test_true
    input = "true"
    assert_equal(true, CustomTextFormat.deserialize(input))
  end

  def test_false
    input = "false"
    assert_equal(false, CustomTextFormat.deserialize(input))
  end

  def test_string
    input = "'true'"
    assert_equal("true", CustomTextFormat.deserialize(input))
  end

  def test_string_with_escaped_single_quotes
    input = "'\'a\' is for \'apple\''"
    assert_equal("'a' is for 'apple'", CustomTextFormat.deserialize(input))
  end

  def test_string_with_escaped_slash
    input = "'this\\that'"
    assert_equal("this\\that", CustomTextFormat.deserialize(input))
  end

  def test_empty_string
    input = ""
    assert_equal("", CustomTextFormat.deserialize(input))
  end

  def test_array_of_booleans
    input = "[true, false]"
    assert_equal([true, false], CustomTextFormat.deserialize(input))
  end

  def test_array_of_strings
    input = "['a', 'b', 'c']"
    assert_equal(["a", "b", "c"], CustomTextFormat.deserialize(input))
  end

  def test_array_of_strings_with_single_quotes
    input = "['\'a\' is for \'apple\'', '\'b\' is for \'banana\'', 'c']"
    expected = ["'a' is for 'apple'", "'b' is for 'banana'", "c"]
    assert_equal(expected, CustomTextFormat.deserialize(input))
  end

  def test_mixed_array
    input = "[true, 'true', '\'a\' is for \'apple\'']"
    expected = [true, "true", "'a' is for 'apple'"]
    assert_equal(expected, CustomTextFormat.deserialize(input))
  end

  def test_empty_array
    input = "[]"
    assert_equal([], CustomTextFormat.deserialize(input))
  end

  def test_nested_array
    input = "[[true], ['a', 'b']]"
    expected = [[true], ['a', 'b']]
    assert_equal(expected, CustomTextFormat.deserialize(input))
  end

  def test_some_arrays_and_some_other_things
    input = "['a', true, [true], ['a', 'b']]"
    expected = ["a", true, [true], ['a', 'b']]
    assert_equal(expected, CustomTextFormat.deserialize(input))
  end

  def test_array_with_empty_strings
    input = "['', 'a', '']"
    expected = ["", "a", ""]
    assert_equal(expected, CustomTextFormat.deserialize(input))
  end
end
