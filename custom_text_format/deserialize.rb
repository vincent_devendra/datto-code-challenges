module CustomTextFormat
  def self.deserialize(input)
    Deserializer.new(input).run
  end

  private

  class Deserializer < Struct.new(:input)
    class InvalidFormatError < StandardError; end

    def run
      raise InvalidFormatError, error_message if invalid_input?

      return deserialized_input unless serialized_array?(input)
      deserialized_array(input)
    end

    def invalid_input?
      input =~ /[^A-Za-z\[\]\\'\,\s]/
    end

    alias_method :invalid_index, :invalid_input?

    def elements(serialized_array)
      serialized_array.gsub(/\A\[|\]\z/, "").split(/,\s+(?![^\[]*\])/)
    end

    def deserialized_array(serialized_array)
      elements(serialized_array).map do |element|
        if serialized_array?(element)
          deserialized_array(element)
        else
          deserialized_element(element)
        end
      end
    end

    def deserialized_element(element = input)
      return true if element == "true"
      return false if element == "false"
      return element if element.empty?
      element[0] = ""
      element[-1] = ""
      element
    end

    alias_method :deserialized_input, :deserialized_element

    def serialized_array?(element)
      element.include?("[") && element.include?("]")
    end

    def error_message
      "Input cannot contain #{input[invalid_index]}"
    end
  end
end
